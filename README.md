# ConwaysGameOfLife

Write a version of Conway’s Game of Life (http://en.wikipedia.org/wiki/Conway’s_Game_of_Life)
that generates a random placement of cells as it’s initial state and iterates
through generations displaying each state. Your program should allow for a
configurable board size and number of generations.

## Prerequisites

Download and install:
- [Node JS](https://nodejs.org/dist/v12.18.4/node-v12.18.4-x64.msi) v12.18.4
- [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3.

## Development server

- First restore the 3rd party packages by running `npm install`.
- Run `ng serve` or `npm start` to launch the dev server. 
- Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.