import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { GameState } from 'src/shared/models/game-configuration';
import { Presets as GamePresets } from 'src/shared/models/presets-enum';
import { GameOfLifeService } from 'src/shared/services/game-of-life.service';

@Component({
    selector: 'app-game-of-life-controls',
    templateUrl: './game-of-life-controls.component.html',
    styleUrls: ['./game-of-life-controls.component.scss'],
})
export class GameOfLifeControlsComponent implements OnInit, OnDestroy {

    // Make Enum Available to Template
    GamePresets = GamePresets;
    selectedPreset: GamePresets;

    // Game State
    gameState: GameState;
    gameStateSubscription: Subscription;
    simulationStoppedSubscription: Subscription;

    // Local Variables
    simulationRunning = false;

    // Local State
    rows: number;
    cols: number;
    speedInMs: number;
    generationsToSimulate: number;

    constructor(private gameOfLifeService: GameOfLifeService, private toastr: ToastrService) { }

    ngOnInit(): void {
        this.updateGameState(this.gameOfLifeService.getGameState());

        this.gameStateSubscription = this.gameOfLifeService.gameStateChanged().subscribe(state => {
            console.log('ngOnInit gamestate changed', state);
            this.updateGameState(state);
        });

        this.simulationStoppedSubscription = this.gameOfLifeService.simulationStopped().subscribe(_ => {
            this.simulationRunning = false;
        });
    }

    ngOnDestroy(): void {
        this.stopSimulation();
        this.gameStateSubscription.unsubscribe();
        this.simulationStoppedSubscription.unsubscribe();
    }

    loadPreset(): void {
        if (!this.selectedPreset) {
            this.toastr.warning(`Please select a preset.`);
            return;
        }

        this.gameOfLifeService.loadPreset(this.selectedPreset);
    }

    exportGameConfiguration(): void {
        this.gameOfLifeService.exportGameConfiguration();
    }

    startSimulation(): void {
        this.gameOfLifeService.startSimulation();
        this.simulationRunning = true;
    }

    stopSimulation(): void {
        this.gameOfLifeService.stopSimulation();
        this.simulationRunning = false;
    }

    applyNewGameSettings(): void {
        this.gameOfLifeService.configureGame(this.rows, this.cols, this.speedInMs, this.generationsToSimulate);
    }

    private updateGameState(state: GameState): void {
        this.gameState = state;
        this.rows = state.rows;
        this.cols = state.cols;
        this.speedInMs = state.speedInMs;
        this.generationsToSimulate = state.generationsToSimulate;
    }
}
