import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { GameOfLifeService } from 'src/shared/services/game-of-life.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Conways Game Of Life';

  constructor(private gameOfLifeService: GameOfLifeService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.gameOfLifeService.configureGame();
  }

  ngOnDestroy(): void {

  }
}
