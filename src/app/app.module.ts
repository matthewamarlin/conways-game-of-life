import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from 'src/shared/modules/mateial-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from 'src/shared/share.module';
import { ToastrModule } from 'ngx-toastr';
import { GameOfLifeControlsComponent } from './game-of-life-controls/game-of-life-controls.component';
import { GameOfLifeGridComponent } from './game-of-life-grid/game-of-life-grid.component';

@NgModule({
  declarations: [
    AppComponent,
    GameOfLifeControlsComponent,
    GameOfLifeGridComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    SharedModule.forRoot(),
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
