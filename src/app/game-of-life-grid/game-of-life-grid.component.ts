import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { GameState } from 'src/shared/models/game-configuration';
import { StatusEnum } from 'src/shared/models/status-enum';
import { GameOfLifeService } from 'src/shared/services/game-of-life.service';

@Component({
    selector: 'app-game-of-life-grid',
    templateUrl: './game-of-life-grid.component.html',
    styleUrls: ['./game-of-life-grid.component.scss'],
})
export class GameOfLifeGridComponent implements OnInit, OnDestroy {

    StatusEnum = StatusEnum;

    gameState: GameState;
    gameStateSubscription: Subscription;

    constructor(private gameOfLifeService: GameOfLifeService) {
    }

    ngOnInit(): void {
        this.gameState = this.gameOfLifeService.getGameState();

        this.gameStateSubscription = this.gameOfLifeService.gameStateChanged().subscribe(state => {
            this.gameState = state;
        });
    }

    toggleState(row: number, col: number): void {
        this.gameOfLifeService.toggleCellState(row, col);
    }

    ngOnDestroy(): void {
        this.gameStateSubscription.unsubscribe();
    }
}
