import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule, PercentPipe, CurrencyPipe, DecimalPipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { GameOfLifeService } from './services/game-of-life.service';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    // Shared Components / Directives
    declarations: [],
    exports: [],
    providers: [
        CurrencyPipe,
        PercentPipe,
        DecimalPipe
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders<SharedModule> {
        return {
            ngModule: SharedModule,
            // Shared Services
            providers: [
                GameOfLifeService
            ]
        };
    }
}
