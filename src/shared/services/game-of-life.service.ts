import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { interval, Observable, Subject, Subscription } from 'rxjs';
import { GameState } from '../models/game-configuration';
import { StatusEnum } from '../models/status-enum';
import * as _ from 'lodash';

@Injectable()
export class GameOfLifeService implements OnDestroy {
    // Constants
    private readonly DEFAULT_GRID_COLS = 50;
    private readonly DEFAULT_GRID_ROWS = 50;
    private readonly DEFAULT_SPEED_IN_MS = 500;
    private readonly DEFAULT_GENERATIONS_TO_SIMULATE = 0;

    // Initial Game State
    private gameState: GameState = {
        grid: null,
        rows: this.DEFAULT_GRID_ROWS,
        cols: this.DEFAULT_GRID_COLS,
        currentGeneration: 0,
        generationsToSimulate: 0,
        speedInMs: 500
    };

    // Simulation Subscription
    simulationSubscription: Subscription;

    // A subject to allow listners to observe the game state change as the simulation runs
    private gameStateChangedSubject: Subject<GameState>;
    private simulationsStoppedSubject: Subject<any>;

    constructor(private http: HttpClient) {
        // Initialise the game state subject
        this.gameStateChangedSubject = new Subject<GameState>();
        this.simulationsStoppedSubject = new Subject<GameState>();
    }

    /**
     * Reconfigures the game using the given parameters.
     * @param rows The amount of rows
     * @param cols  the amount of cols
     * @param speedInMs the speed of the simulation in milliseconds
     * @param generationsToSimulate the amount of generations to simulate
     */
    configureGame(rows = this.DEFAULT_GRID_ROWS,
                  cols = this.DEFAULT_GRID_COLS,
                  speedInMs = this.DEFAULT_SPEED_IN_MS,
                  generationsToSimulate = this.DEFAULT_GENERATIONS_TO_SIMULATE): GameState {
        const newGrid = this.createNewGrid(rows, cols, this.gameState.grid);

        this.gameState.speedInMs = speedInMs;
        this.gameState.grid = newGrid;
        this.gameState.rows = rows;
        this.gameState.cols = cols;
        this.gameState.generationsToSimulate = generationsToSimulate;
        this.gameState.speedInMs = speedInMs;

        this.triggerGameStateChanged();
        return this.gameState;
    }

    /**
     * Gets the current game state.
     */
    getGameState(): GameState {
        if (!this.gameState) {
            throw Error('Game of life has not been initialised');
        }

        return _.clone(this.gameState);
    }

    /**
     * Loads a preset game configuration
     * @param presetName The preset to load
     */
    loadPreset(presetName: string): Observable<any> {
        const uri = `assets/presets/${presetName}`;

        const gameStateLoaded = this.http.get<GameState>(uri);

        gameStateLoaded.subscribe(result => {
            console.log('result', result);
            this.gameState.rows = result.grid.length;
            this.gameState.cols = result.grid[0].length;
            this.gameState.grid = result.grid;
            this.gameState.speedInMs = result.speedInMs;
            this.triggerGameStateChanged();
        });

        return gameStateLoaded;
    }

    /**
     * Starts the simulation
     */
    startSimulation(): void {
        this.gameState.currentGeneration = 0;

        if (!this.simulationSubscription || this.simulationSubscription.closed) {
            this.simulationSubscription = interval(this.gameState.speedInMs)
                .subscribe(() => {
                    if (this.gameState.generationsToSimulate > 0) {
                        if (this.gameState.currentGeneration === this.gameState.generationsToSimulate) {
                            this.stopSimulation();
                            // TODO Make Completed hook
                            // tslint:disable-next-line: max-line-length
                            // this.toastr.success('Simulation Finished', `The simulation reached ${this.generationsToSimulate} generations.`);
                            return;
                        }
                    }

                    this.processNextGeneration();
                    this.gameState.currentGeneration++;
                    this.triggerGameStateChanged();
                });
        }
    }

    ngOnDestroy(): void {
        // Unsubscribe from all Subscriptions.
        this.stopSimulation();
    }

    /**
     * An observable that allows listners to observe the game state as it changes. 
     */
    gameStateChanged(): Observable<GameState> {
        return this.gameStateChangedSubject.asObservable();
    }

    /**
     * An observable that allows listners to observe when the simulation has ended. 
     */
    simulationStopped(): Observable<GameState> {
        return this.simulationsStoppedSubject.asObservable();
    }

    /**
     * Toggles the state of a specific cell
     */
    toggleCellState(row: number, col: number): void {
        this.gameState.grid[row][col] = this.gameState.grid[row][col] === StatusEnum.Alive ? StatusEnum.Dead : StatusEnum.Alive;
    }

    /**
     * Stops the simulation
     */
    stopSimulation(): void {
        this.simulationsStoppedSubject.next();
        this.simulationSubscription.unsubscribe();
        this.simulationSubscription = null;
    }

    /**
     * Exports the current game configuration and downloads it to a file.
     */
    exportGameConfiguration(): void {
        this.saveJSON(this.gameState, 'game_config.json');
    }

    /**
     * Triggers a subject to emit that the game state has changed to listners.
     */
    private triggerGameStateChanged(): void {
        this.gameStateChangedSubject.next(_.clone(this.gameState));
    }

    /**
     * Processes the next generation for the grid
     */
    private processNextGeneration(): void {
        // console.log('Processing Next Generation');
        if (this.gameState.grid) {
            const nextGeneration = this.createNewGrid(this.gameState.rows, this.gameState.cols);

            // Iterate Over Each Row
            this.gameState.grid.forEach((row, rowIndex) => {
                // Iterate Over Each Column
                row.forEach((_, colIndex) => {
                    nextGeneration[rowIndex][colIndex] = this.computeCellsNextGeneration(rowIndex, colIndex);
                });
            });

            this.gameState.grid = nextGeneration;
        }
    }

    /**
     * Computes the next generation for a given cell
     * @param rowIndex The row index of the cell
     * @param colIndex The col index of the cell
     */
    private computeCellsNextGeneration(rowIndex: number, colIndex: number): StatusEnum {

        const currentCell = this.gameState.grid[rowIndex][colIndex];
        let aliveNeighbors = 0;

        const rowStart = Math.max(rowIndex - 1, 0);
        const rowFinish = Math.min(rowIndex + 1, this.gameState.rows - 1);
        const colStart = Math.max(colIndex - 1, 0);
        const colFinish = Math.min(colIndex + 1, this.gameState.cols - 1);

        for (let curRow = rowStart; curRow <= rowFinish; curRow++) {
            for (let curCol = colStart; curCol <= colFinish; curCol++) {
                aliveNeighbors += this.gameState.grid[curRow][curCol] === StatusEnum.Alive ? 1 : 0;
            }
        }

        // The current cell needs to be subtracted
        // from its neighbors as it was
        // counted before
        aliveNeighbors -= currentCell === StatusEnum.Alive ? 1 : 0;

        console.log(`Computing Life Rules For Cell: ${rowIndex + 1}, ${colIndex + 1} | status = ${currentCell} | living neightbours = ${aliveNeighbors}`);

        /*
          Life Rules:

          Any live cell with fewer than two live neighbours dies, as if by underpopulation.
          Any live cell with two or three live neighbours lives on to the next generation.
          Any live cell with more than three live neighbours dies, as if by overpopulation.
          Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

          Simplified to:

          Any live cell with two or three live neighbours survives.
          Any dead cell with three live neighbours becomes a live cell.
          All other live cells die in the next generation. Similarly, all other dead cells stay dead.
        */

        // Any live cell with two or three live neighbours survives.
        if (currentCell === StatusEnum.Alive && (aliveNeighbors === 2 || aliveNeighbors === 3)) {
            return StatusEnum.Alive;
        }

        // Any dead cell with three live neighbours becomes a live cell.
        if (currentCell === StatusEnum.Dead && aliveNeighbors === 3) {
            return StatusEnum.Alive;
        }

        // All other live cells die in the next generation. Similarly, all other dead cells stay dead.
        return StatusEnum.Dead;
    }

    /**
     * Constructs an empty grid given the rows and cols.
     * @param rows The amount of rows
     * @param cols The amount of cols
     */
    private createNewGrid(rows: number, cols: number, exsistingGrid: StatusEnum[][] = null): Array<Array<StatusEnum>> {
        const newGrid = [];

        for (let i = 0; i < rows; i++) {
            newGrid[i] = [];
            for (let j = 0; j < cols; j++) {
                newGrid[i][j] = StatusEnum.Dead;
            }
        }

        if (exsistingGrid) {
            for (let r = 0; r < rows; r++) {
                for (let c = 0; c < cols; c++) {
                    if (!exsistingGrid[r]) {
                        newGrid[r][c] = StatusEnum.Dead;
                    }
                    else {
                        if (!exsistingGrid[r][c]) {
                            newGrid[r][c] = StatusEnum.Dead;
                        }
                        else {
                            newGrid[r][c] = exsistingGrid[r][c];
                        }
                    }
                }
            }
        }

        return newGrid;
    }

    /**
     * A function to save a JSON object to file, adapted from http://bgrins.github.io/devtools-snippets/#console-save
     * @param data The Json data to save
     * @param filename The file name
     */
    private saveJSON(data: any, filename: string): void {
        if (!data) {
            console.error('No data');
            return;
        }

        if (!filename) { filename = 'console.json'; }

        if (typeof data === 'object') {
            data = JSON.stringify(data, undefined, 4);
        }

        const blob = new Blob([data], { type: 'text/json' });
        const e = document.createEvent('MouseEvents');
        const a = document.createElement('a');

        a.download = filename;
        a.href = window.URL.createObjectURL(blob);
        a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
        e.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        a.dispatchEvent(e);
    }
}
