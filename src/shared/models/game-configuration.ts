import { StatusEnum } from './status-enum';

export interface GameState {
    grid: StatusEnum[][];
    rows: number;
    cols: number;
    currentGeneration: number;
    generationsToSimulate: number;
    speedInMs: number;
}
